import { Component, OnInit, AfterViewInit } from '@angular/core';
import { TableAdjointService } from 'src/app/services/table-adjoint.service';
import { IRecord } from 'src/app/models/record.model';
import { element } from 'protractor';
import { DataTable } from 'src/app/models/dataTable.model';
import { RecordHeader } from 'src/app/models/recordHeaderTable.model';
import { resizeOriginEnum } from 'src/app/models/resizeOrigin.enum';
import { DataDisplayParams } from 'src/app/models/tableDataDisplayParams';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent<T,U> implements OnInit, AfterViewInit {
  public headerColDef : DataTable<RecordHeader> [] ;
  public dataTab : DataTable<IRecord>[] ;
  public records: IRecord[] ;

  constructor(
    private _adjoint: TableAdjointService,
  ) { }

  ngOnInit() {
    this.headerColDef = this._adjoint.getColDef() ;
    this._adjoint.getRecord().subscribe(
      (response: IRecord[]) => {
        this.records = response;
        this.initdataTab()
        console.log(this.records)
      }
    )
    console.log("I'm the Boss", this.headerColDef)
  }

  public initdataTab(): void {
    this.dataTab = []
    this.records.forEach(rec => {
      var displayParams : DataDisplayParams = {
        left : null,
        width : null,
        widthNumber: null
      }
      this.dataTab.push(new DataTable<IRecord>(rec, rec.id, "rec"+rec.id.toString() ,displayParams)) 
    })
    console.log(this.dataTab)
    
  }

  ngAfterViewInit() {
    
  }

}
