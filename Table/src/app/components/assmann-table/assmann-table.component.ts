import { Component, OnInit, Input } from '@angular/core';
import { DataTable } from 'src/app/models/dataTable.model';
import { resizeOriginEnum } from 'src/app/models/resizeOrigin.enum';
import { tableFunctionEnum } from 'src/app/models/tableFunctions.enum';
import { templateJitUrl } from '@angular/compiler';

@Component({
  selector: 'app-assmann-table',
  templateUrl: './assmann-table.component.html',
  styleUrls: ['./assmann-table.component.scss']
})
export class AssmannTableComponent<T, U> implements OnInit {
  @Input() headerColDef: DataTable<T>[];
  @Input() displayedData: DataTable<U>[];
  @Input() selection: boolean;
  @Input() sort: boolean;
  @Input() resize: boolean;
  @Input() dragndrop: boolean;

  public colDraged: DataTable<T>;
  public colResized: DataTable<T>;
  public colResizedRef: DOMRect;
  public isDraging: boolean;
  public isResizing: boolean;
  public isSorting: boolean;
  public resizeOrigin: resizeOriginEnum;
  public totalWidth: number;
  public selectWidth: number;


  constructor() { }

  ngOnInit(): void {
    this.initBooleans();
    this.resizeOrigin = null;
    this.initItemContainers();
    this.getTotalWidth();
    this.initSelectCol();
    console.log("header", this.headerColDef)
    console.log("rec", this.displayedData)
  }

  private initBooleans(): void {
    this.isDraging = false;
    this.isResizing = false;
    this.isSorting = false;
  }

  private initItemContainers(): void {
    this.colDraged = null;
    this.colResized = null;
    this.colResizedRef = null;
  }

  private getDomRect(item: DataTable<T | U>): DOMRect {
    return document.getElementById(item.id.toString()).getBoundingClientRect();
  }

  private initSelectCol(): void {
    if (this.selection) {
      this.selectWidth = 60;
    }
  }

  private getOriginOfResize(event: MouseEvent, ref: DOMRect): resizeOriginEnum {
    var midle = ref.x + (ref.width / 2);
    if (event.x >= ref.x && event.x < midle) {
      return resizeOriginEnum.left;
    } else {
      return resizeOriginEnum.right;
    }
  }

  public getTotalWidth(): number {
    this.totalWidth = 0;
    this.headerColDef.forEach(col => {
      this.totalWidth = this.totalWidth + col.displayParams.widthNumber
    })
    this.totalWidth = this.totalWidth + this.selectWidth
    return this.totalWidth
  }

  public mouseDown(event: MouseEvent, contextCol: DataTable<T>): void {
    const htmlRef: DOMRect = this.getDomRect(contextCol);
    console.log(event, htmlRef)
    if (event.x > htmlRef.left + 10 && event.x < htmlRef.right - 10) {
      setTimeout(() => {
        if (!this.isDraging && !this.isSorting) {
          this.drag(contextCol);
        } else {
          this.isDraging = false;
          this.isSorting = false;
        }
      }, 90)
    } else {
      setTimeout(() => {
        if (!this.isResizing && !this.isSorting) {
          this.resizeOrigin = this.getOriginOfResize(event, htmlRef);
          this.isResizing = true;
          this.startResize(contextCol, htmlRef);
        } else {
          this.isResizing = false;
          this.isSorting = false;
        }
      }, 90)
    }

  }

  private drag(itemMouving: DataTable<T>): void {
    if (this.dragndrop) {
      this.isDraging = true;
      this.colDraged = itemMouving;
      this.headerColDef = this.headerColDef.filter(col => col !== itemMouving);
    }
  }

  private startResize(colResizing: DataTable<T>, colRef: DOMRect): void {
    if (this.resize) {
      this.isResizing = true;
      this.colResized = colResizing;
      this.colResizedRef = colRef;
    }
  }

  public moveItem(event: MouseEvent): void {
    if (this.isDraging) {
      var item = document.getElementById("followMouse");
      item.style.top = (event.y - item.parentElement.offsetTop).toString() + 'px';
      item.style.left =( event.x - item.parentElement.offsetLeft).toString() + 'px';
    } else if (this.isResizing) {
      var item = document.getElementById('followToResize');
      item.style.left =( event.x - item.parentElement.offsetLeft).toString() + 'px';
      item.style.top = (this.colResizedRef.top - item.parentElement.offsetTop).toString() + 'px';
      item.style.height = this.colResizedRef.height.toString() + "px";
    }
  }

  public mouseUp(call: tableFunctionEnum, event: MouseEvent, col?: DataTable<T>): void {
    switch (call) {
      case tableFunctionEnum.sort:
        this.sortCol(col);
        break;
      case tableFunctionEnum.resize:
        this.stopResize(event);
        break
      case tableFunctionEnum.dragndrop:
        this.findCase(event);
        break;
      default:
        break;
    }
  }

  private sortCol(sortCol: DataTable<T>): void {
    this.isSorting = true;
    this.setSticky(sortCol)
    if (this.sort) {
      console.log("sort", sortCol)
    }
  }

  private findCase(event: MouseEvent): void {
    if (this.dragndrop) {
      this.headerColDef.forEach(col => {
        var item = document.getElementById(col.id.toString());
        if (item) {
          if (event.x > item.getBoundingClientRect().x && event.x < (item.getBoundingClientRect().x + item.clientWidth)) {
            this.drop(event, col);
          };
        } else if (item && col.id == this.headerColDef.length && event.x > item.getBoundingClientRect().x) {
          this.drop(event, col);
        };
      });
    } else {
      console.log("juste click");
    }
  }

  private drop(event: MouseEvent, col: DataTable<T>): void {
    var element = document.getElementById(col.id.toString());
    this.isDraging = false;
    var newTab = this.headerColDef.filter(col => col !== this.colDraged);
    if (event.x <= (element.getBoundingClientRect().x + element.clientWidth / 2)) {
      newTab.splice(newTab.indexOf(col), 0, this.colDraged);
    } else if (event.x > (element.getBoundingClientRect().x + element.clientWidth / 2)) {
      newTab.splice(newTab.indexOf(col) + 1, 0, this.colDraged);
    }
    this.headerColDef = this.refreshIdOfHeaderCol(newTab).map(col => col);
    this.colDraged = null;
  }

  private refreshIdOfHeaderCol(table: DataTable<T>[]): DataTable<T>[] {
    var i = 0;
    while (i < table.length) {

      table[i].id = i;
      i++
    }
    return table
  }

  private stopResize(event: MouseEvent) {
    if (this.resize) {
      this.headerColDef.forEach(col => {
        if (col.id == this.colResized.id) {
          var newWidth = this.calculNewWidth(this.colResizedRef, event)
          console.log(newWidth)
          col.displayParams.width = newWidth.toString() + "px"
          col.displayParams.widthNumber = newWidth;
        }
      })
      this.isResizing = false
    }
  }

  private calculNewWidth(ref: DOMRect, event: MouseEvent): number {
    var newWidth: number;
    if (event.x < ref.x && this.resizeOrigin == resizeOriginEnum.left) {
      newWidth = (ref.x - event.x) + ref.width;
    } else if (event.x > ref.x && this.resizeOrigin == resizeOriginEnum.left) {
      var partToDelete = event.x - ref.x;
      newWidth = ref.width - partToDelete
    } else {
      newWidth = event.x - ref.x
    }
    return Math.floor(newWidth)
  }

  public select(data: DataTable<U>): void {
    if (this.selection) {
      console.log("select", data.data)
    }
  }

  public setSticky(Hdata: DataTable<T | U>) {
    Hdata.isSticky = !Hdata.isSticky;
    if (Hdata.isSticky) {
      Hdata.displayParams.left = this.getLeft(Hdata).toString() + "px";
      document.getElementById(Hdata.id.toString()).style.left = Hdata.displayParams.left;
    } else {
      Hdata.displayParams.left = null;
      document.getElementById(Hdata.id.toString()).style.left = Hdata.displayParams.left;
    }
  }

  private getLeft(Hdata: DataTable<T | U>): number {
    var href = document.getElementById(Hdata.id.toString()) ;
    var left : number = href.getBoundingClientRect().left - href.parentElement.getBoundingClientRect().left  ;
    return left ;
  }

  public isSticky(data: DataTable<T>): boolean {
    return data.isSticky ? true : false;
  }
}