import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssmannTableComponent } from './assmann-table.component';

describe('AssmannTableComponent', () => {
  let component: AssmannTableComponent;
  let fixture: ComponentFixture<AssmannTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssmannTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssmannTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
