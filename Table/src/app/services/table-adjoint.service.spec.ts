import { TestBed } from '@angular/core/testing';

import { TableAdjointService } from './table-adjoint.service';

describe('TableAdjointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TableAdjointService = TestBed.get(TableAdjointService);
    expect(service).toBeTruthy();
  });
});
