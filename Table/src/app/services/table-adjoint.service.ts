import { Injectable } from '@angular/core';
import { TableManagerService } from './table-manager.service';
import { Observable } from 'rxjs';
import { IRecord } from '../models/record.model';

@Injectable({
  providedIn: 'root'
})
export class TableAdjointService {

  constructor(
    private _manager : TableManagerService,
  ) { }

  public getRecord(): Observable<IRecord[]> {
    return this._manager.getRecords() ;
  }
  
  public sortRecords(records : any[]): void {
    console.log("sort table")
  }

  public getColDef() {
   return this._manager.getColDef()
  }
}
