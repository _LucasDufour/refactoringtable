import { Injectable } from '@angular/core';
import { RecordsApiService } from '../fake-services/records-api.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { IRecord } from '../models/record.model';
import { DataTable } from '../models/dataTable.model';
import { RecordHeader } from '../models/recordHeaderTable.model';
import { DataDisplayParams } from '../models/tableDataDisplayParams';

@Injectable({
  providedIn: 'root'
})
export class TableManagerService {

  constructor(
    private _api : RecordsApiService,
  ) { }

  public getRecords(): Observable<IRecord[]> {
    var obs = this._api.fetchRecord().pipe(
      tap(x => {
        console.log(x)
      })
    )
    console.log(obs)
    return obs ;
  }

  public getColDef() : DataTable<RecordHeader>[] {
    var dataTable : DataTable<RecordHeader>[] = [];
    var recordHeaderTab : RecordHeader[] = [
      { displayName: "id", id: 0, width: 60 },
      { displayName: "channel_id", id: 1, width: 100 },
      { displayName: "channel_name", id: 2, width: 100 },
      { displayName: "duration", id: 3, width: 100 },
      { displayName: "direction", id: 4, width: 100 },
      { displayName: "start_date_time", id: 5, width: 200 },
      { displayName: "stop_date_time", id: 6, width: 200 },
      { displayName: "calling_party_number", id: 7, width: 200 },
      { displayName: "called_party_number", id: 8, width: 200 }
    ]
    recordHeaderTab.forEach(col => {
      var params : DataDisplayParams = {
        left : null,
        width : col.width.toString()+"px",
        widthNumber : col.width
      }
      dataTable.push(new DataTable(col, col.id, col.displayName,params))
    })
    return dataTable
  }
}
