import { TestBed } from '@angular/core/testing';

import { RecordsApiService } from './records-api.service';

describe('RecordsApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecordsApiService = TestBed.get(RecordsApiService);
    expect(service).toBeTruthy();
  });
});
