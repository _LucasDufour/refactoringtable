import { Injectable } from '@angular/core';
import { IRecord } from '../models/record.model';
import { Observable, of, range } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecordsApiService {

  constructor() { }

  public fetchRecord(): Observable<IRecord[]> {
    return of(this.initFakeRecord())
  }

  private initFakeRecord(): IRecord[] {
    var recordsTab : IRecord[] = [] ;
    while (recordsTab.length < 100) {
      recordsTab.push({id : recordsTab.length, channel_id : 0, duration :10, direction: 1, start_date_time : 0, stop_date_time: 0, channel_name: 'name', called_party_number: 'h', calling_party_number: 'e'})
    }
    return recordsTab ;
  };
}
