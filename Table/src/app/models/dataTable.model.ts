import { DataDisplayParams } from './tableDataDisplayParams';

export class DataTable<T>  {
    public isSticky : boolean ;
    public isSelected : boolean ;
    public displayParams : DataDisplayParams ;
    public data : T ;
    public displayName : string ;
    public id : number ; 

    constructor(inputData : T,id : number, displayName : string, displayParams? : DataDisplayParams) {
        this.isSticky = false ;
        this.isSelected = false ;
        this.data = inputData ;
        this.id = id ;
        this.displayParams = displayParams ;
        this.displayName = displayName ;
    }  

    public reverseStickyStatus() : void {
        this.isSticky = !this.isSticky ;
    }

    public reverseSelectedStatus(): void {
        this.isSelected = !this.isSelected ;
    }

}