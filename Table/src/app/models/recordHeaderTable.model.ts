export type RecordHeader = {
    displayName : string ;
    id : number ;
    width : number ;
}