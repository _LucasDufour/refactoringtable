export type DataDisplayParams = {
    width : string ;
    widthNumber : number ;
    left : string | null ;
}