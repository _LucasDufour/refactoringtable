export interface IRecord {
    id: number;
    channel_id: number;
    channel_name: string;
    duration: number;
    direction: number;
    start_date_time: number;
    stop_date_time: number;
    calling_party_number: string;
    called_party_number: string;
}